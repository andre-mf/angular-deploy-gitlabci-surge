# Deploy de aplicação Angular no serviço de hospedagem gratuita Surge.sh

Exemplo de deploy de aplicação Angular por meio do GitLab CI

## Adicionando o Surge às dependências do projeto
```shell
npm install surge
``` 
## Criação da _pipeline_ no arquivo .gitlab-ci.yml
```
image: node
before_script:
  - npm install @angular/cli@13.1.2
  - npm install
  - npm run build
deploy:
  stage: deploy
  script:
    - cp CNAME dist/angular-deploy-gitlabci/
    - node_modules/.bin/surge dist/angular-deploy-gitlabci
```

## Criação do arquivo **CNAME** com o domínio
```shell
echo angular-deploy-gitlabci.surge.sh > CNAME
``` 

## Adicionando as variáveis do Surge ao repositório
- No repositório do projeto no GitLab, configure as variáveis para deploy no Surge.
> Settings - CI/CD - Variables - Expand - Add variable

> Key (SURGE_LOGIN) - Value (e-mail cadastrado no Surge) - Add variable

> Key (SURGE_TOKEN) - Value (token do surge - utilize surge token para obter) - Add variable

